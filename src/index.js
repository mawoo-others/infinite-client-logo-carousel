// Bootstrap JavaScript
import "bootstrap";

// Custom styles
import "./css/styles.scss";

// Custom scripts
import Carousel from "./js/carousel";

(() => {
  if (typeof $ === "undefined") {
    throw new TypeError("... requires jQuery.");
  }

  $(document).ready(() => {
    const carousel = new Carousel($("#carousel"));

    carousel.init();
  });
})();
